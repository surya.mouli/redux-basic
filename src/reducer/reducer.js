import {rewriteVal,
    imageLoading} from '../action/action'
const reducer = function(state,action){
    switch(action.type){
        case rewriteVal: 
        return {...state,clicked:true,imageLoaded:!state.imageLoaded}

        case imageLoading:
        return {...state,imageLoaded:!state.imageLoaded}

        default:
        return state;
    }
}

export default reducer
