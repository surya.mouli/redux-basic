import React, { Component } from 'react';
import Header from './Header';
import Input from './Input'
import Footer from './Footer'

class App extends Component {
  render() {
    return (
      <div>
        <Header/>
        <Input/>
        <Footer/>
      </div>
    );
  }
}

export default App;
