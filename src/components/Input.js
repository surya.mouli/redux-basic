import React from 'react';
import {connect} from 'react-redux'
import '../styles/input.css'
class Input extends React.Component{
    handleClick=()=>{
        this.props.dispatch({type:'IMAGE_LOADED'})
        window.setTimeout(this.toggle,2000)
        console.log(this.props)
    }
    toggle = () => {
        // this.props.dispatch({type:'IMAGE_LOADED'})
        this.props.dispatch({type:'REWRITE_VAL'})
        console.log(this.props)
    }
    render(){
        let result
        let image
        if(this.props.clicked){
            result = 
            <div>
                <h1>Loading Successful</h1>
                <input type = 'button' value='click me' onClick={this.handleClick} className='input-btn'></input>
            </div>
        }
        else{
          result = <input type = 'button' value='click me' onClick={this.handleClick} className='input-btn'></input>
        }
        if(this.props.imageLoaded){
            image=<img src="https://media2.giphy.com/media/y1ZBcOGOOtlpC/200.gif"></img>
            result=<h3>Loading ...</h3>
        }
        return(
            <div className='input-div'>
                {image}
                {result}
            </div>

        )
    }
}
const mapStateToProps = (state)=>{
    return state
}
export default connect(mapStateToProps)(Input);